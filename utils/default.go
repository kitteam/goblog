package utils

import (
	"math/rand"
)

// 函　数：生成随机数
// 概　要：
// 参　数：
//      min: 最小值
//      max: 最大值
// 返回值：
//      int64: 生成的随机数
func RandInt64(min, max int64) int64 {
	if min >= max || min == 0 || max == 0 {
		return max
	}
	return rand.Int63n(max-min) + min
}

// 转成API结果集
func (response *Response) ToAPIResult() map[string]interface{} {
	return map[string]interface{}{
		"rel":        response.Success,
		"msg":        response.Message,
		"statusCode": response.StatusCode,
		"data":       response.Data,
		"count":      response.Count,
	}
}
// 响应结构
type Response struct {
	Success    bool
	Message    string
	StatusCode int
	Data       interface{}
	Count      int
}
// 创建响应
func CreateResponse(success bool, statusCode int) *Response {
	response := &Response{}
	response.Success = success
	response.StatusCode = statusCode
	return response
}
// 返回错误Response
func ErrorResponse(err error) *Response {
	response := &Response{}
	response.Success = false
	response.Message = err.Error()
	response.StatusCode = 500
	return response
}