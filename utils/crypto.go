package utils

import (
	"crypto/sha256"
	"encoding/hex"
)
// SHA256加密
func SHA256Hash(str string) string{
	key := []byte(str)
	hash := sha256.New()
	hash.Write(key)
	md := hash.Sum(nil)
	res :=  hex.EncodeToString(md)
	return res
}