package controllers

import (
	"github.com/astaxie/beego"
	"gitee.com/kitteam/goblog/utils"
)

type ControllerBase struct {
	beego.Controller
}

func (that *ControllerBase) Prepare() {


	// 随机背景色
	randColors := [...]string{"#009688","#009688", "#ff5722", "#393d49", "#1e9fff", "#5fb878", "#3a794b"}
	that.Data["BGColor"] = randColors[utils.RandInt64(1, 7)]
}


type APIControllerBase struct{
	beego.Controller
}

func (that *APIControllerBase) Prepare() {

}