package controllers

import (
	"gitee.com/kitteam/goblog/models"
	"encoding/json"
)


type APIArticleSaveController struct {
	APIControllerBase
}

func (that *APIArticleSaveController) Post() {
	var req models.ArticleSaveRequest
	json.Unmarshal(that.Ctx.Input.RequestBody,&req)
	res := models.SaveArticle(req)

	that.Data["json"] = res.ToAPIResult()
	that.ServeJSON()
}

type APIArticlePagesController struct {
	APIControllerBase
}

func (that *APIArticlePagesController) Post() {
	res := models.QueryArticlePages()
	that.Data["json"] = res.ToAPIResult()
	that.ServeJSON()
}

type APIArticleDetailController struct {
	APIControllerBase
}

func (that *APIArticleDetailController) Get() {
	id := that.Ctx.Input.Param(":id")
	res := models.QueryArticleDetail(id)
	that.Data["json"] = res.ToAPIResult()
	that.ServeJSON()
}