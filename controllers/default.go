package controllers

import (
	"time"
	"gitee.com/kitteam/goblog/models"
)

type MainController struct {
	ControllerBase
}

func (c *MainController) Get() {
	c.Data["Website"] = "GOBLOG"
	c.Data["Email"] = "zheng_jinfan@126.com"
	c.Data["TimeNow"] = time.Now().Format("2006-01-02 15:04:05")

	articles := models.QueryArticlePages().Data
	c.Data["Articles"] = articles
	c.TplName = "index.html"
}

type NotFoundController struct {
	ControllerBase
}

func (c *NotFoundController) Get() {
	c.TplName = "404.html"
}

type AboutController struct {
	ControllerBase
}

func (c *AboutController) Get() {
	c.TplName = "about.html"
}

type CommentController struct {
	ControllerBase
}

func (c *CommentController) Get() {
	c.TplName = "comment.html"
}

type DetailController struct {
	ControllerBase
}

func (c *DetailController) Get() {
	article := models.QueryArticleDetail("5").Data
	c.Data["Article"] = article
	c.TplName = "details.html"
}

type MessageController struct {
	ControllerBase
}

func (c *MessageController) Get() {
	c.TplName = "message.html"
}

