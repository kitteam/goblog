package routers

import (
	"gitee.com/kitteam/goblog/controllers"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/plugins/cors"
)

func init() {
	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "Authorization", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		AllowCredentials: true,
	}))
	// API
	beego.Router("/api/article/create", &controllers.APIArticleSaveController{})
	beego.Router("/api/article/query", &controllers.APIArticlePagesController{})
	beego.Router("/api/article/detail/:id", &controllers.APIArticleDetailController{})


    beego.Router("/", &controllers.MainController{})
	beego.Router("/404", &controllers.NotFoundController{})
	beego.Router("/about", &controllers.AboutController{})
	beego.Router("/comment", &controllers.CommentController{})
	beego.Router("/detail/:id", &controllers.DetailController{})
	beego.Router("/message", &controllers.MessageController{})
}
