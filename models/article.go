package models

import (
	"gitee.com/kitteam/goblog/utils"
	"time"
)

// 读取最新文章
func GetArticles() *utils.Response{
	res := utils.CreateResponse(false,200)
	


	return res
}


type ArticleSaveRequest struct {
	ID         int64
	Title      string
	CategoryId int64
	Status     int64
	Alias      string
	Abstract   string
	Content    string
}

func SaveArticle(model ArticleSaveRequest) *utils.Response {
	res := utils.CreateResponse(false, 200)
	conn, connErr := getDBConnection()
	if connErr != nil {
		return utils.ErrorResponse(connErr)
	}
	defer conn.Close()
	db := conn.GetInstance()
	fields := map[string]interface{}{
		"Title":      model.Title,
		"CategoryId": model.CategoryId,
		"Status":     model.Status,
		"Alias":      model.Alias,
		"Abstract":   model.Abstract,
		"Content":    model.Content,
		"UserId":     1,
		"CreateTime": time.Now(),
	}
	row := 0
	if model.ID == -1 {
		i, err := db.Table("Articles").Data(fields).Insert()
		if err != nil {
			return utils.ErrorResponse(err)
		}
		row = i
	} else {
		u, err := db.Table("Articles").Data(fields).Where("ID", model.ID).Update()
		if err != nil {
			return utils.ErrorResponse(err)
		}
		row = u
	}
	if row > 0 {
		res.Message = "操作成功."
		res.Success = true
	} else {
		res.Message = "操作失败."
	}
	return res
}

type ArticleModel struct {
	ID           int64
	Title        string
	CategoryId   int64
	CategoryName string
	Status       int64
	Alias        string
	Abstract     string
	Content      string
	CreateTime   string
	UserName     string
}
// 查询列表
func QueryArticlePages() *utils.Response{
	res := utils.CreateResponse(false, 200)
	conn, connErr := getDBConnection()
	if connErr != nil {
		return utils.ErrorResponse(connErr)
	}
	defer conn.Close()
	db := conn.GetInstance()
	d, err := db.Query(`SELECT a.ID AS ID,a.Title,c.Name AS CategoryName,a.Status AS Status,a.Alias AS Alias,a.Abstract AS Abstract,a.Content AS Content ,a.CreateTime AS CreateTime,u.UserName AS UserName
			FROM Articles a INNER JOIN Users u ON a.UserId=u.ID INNER JOIN Categories c ON a.CategoryId=c.ID ORDER BY a.ID DESC`)
	if err != nil {
		return utils.ErrorResponse(err)
	}
	if len(d) > 0 {
		var singles []*ArticleModel
		for _, item := range d {
			single := &ArticleModel{}
			single = renderArticleItem(single, item)
			singles = append(singles, single)
		}
		res.Data = singles
		res.Message = "获取成功."
		res.Success = true
	} else {
		res.Message = "暂无数据.."
	}
	return res
}
func QueryArticleDetail(id string) *utils.Response{
	res := utils.CreateResponse(false, 200)
	conn, connErr := getDBConnection()
	if connErr != nil {
		return utils.ErrorResponse(connErr)
	}
	defer conn.Close()
	db := conn.GetInstance()
	d, err := db.Query(`SELECT a.ID AS ID,a.Title,c.Name AS CategoryName,a.Status AS Status,a.Alias AS Alias,a.Abstract AS Abstract,a.Content AS Content ,a.CreateTime AS CreateTime,u.UserName AS UserName,
			c.ID AS CategoryId 
			FROM Articles a INNER JOIN Users u ON a.UserId=u.ID INNER JOIN Categories c ON a.CategoryId=c.ID WHERE a.ID=?`,id)
	if err != nil {
		return utils.ErrorResponse(err)
	}
	if len(d) > 0 {
		single := &ArticleModel{}
		res.Data = renderArticleItem(single, d[0])
		res.Message = "获取成功."
		res.Success = true
	} else {
		res.Message = "暂无数据.."
	}
	return res
}

func renderArticleItem(single *ArticleModel,item map[string]interface{}) *ArticleModel{
	single.ID = item["ID"].(int64)
	single.Title = item["Title"].(string)
	if item["CategoryId"] !=nil {
		single.CategoryId = item["CategoryId"].(int64)
	}
	single.CategoryName = item["CategoryName"].(string)
	single.Status = item["Status"].(int64)
	single.Alias = item["Alias"].(string)
	single.Abstract = item["Abstract"].(string)
	single.Content = item["Content"].(string)
	single.CreateTime = item["CreateTime"].(string)//.Format("2006-01-02 15:04:05")
	single.UserName = item["UserName"].(string)
	return single
}