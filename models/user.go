package models

import (
	"gitee.com/kitteam/goblog/utils"
	"time"
)

// 登录
func Login(name string, pwd string) *utils.Response {
	res := utils.CreateResponse(false, 200)
	conn, err := getDBConnection()
	if err != nil {
		return utils.ErrorResponse(err)
	}
	defer conn.Close()
	db := conn.GetInstance()
	result,qErr := db.Query(`SELECT * FROM Users WHERE UserName=?`, name)
	if qErr != nil {
		return utils.ErrorResponse(qErr)
	}
	if len(result) == 0{
		res.Message = "用户名输入错误."
		return res
	}
	item := result[0]
	password := ""
	if item["Password"] != nil{
		password = item["Password"].(string)
	}
	if password != utils.SHA256Hash(pwd){
		res.Message = "用户名或者密码输入错误."
		return res
	}
	res.Success = true
	res.Message = "登录成功"
	res.Data = map[string]string{
		"UserName":item["UserName"].(string),
		"CreateTime":item["CreateTime"].(time.Time).Format("2006-01-02 15:04:05"),
	}
	return res
}