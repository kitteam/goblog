package models

import (
	"errors"
	"github.com/gohouse/gorose"
	"github.com/astaxie/beego"
	_ "github.com/go-sql-driver/mysql"
)

// 获取配置
func getDbConfig() map[string]interface{} {
	defaultDb := beego.AppConfig.String("default_db")

	devHost := beego.AppConfig.String("mssql_dev_host")
	devUsername := beego.AppConfig.String("mssql_dev_username")
	devPassword := beego.AppConfig.String("mssql_dev_password")
	devDatabase := beego.AppConfig.String("mssql_dev_database")

	prodHost := beego.AppConfig.String("mssql_prod_host")
	prodUsername := beego.AppConfig.String("mssql_prod_username")
	prodPassword := beego.AppConfig.String("mssql_prod_password")
	prodDatabase := beego.AppConfig.String("mssql_prod_database")


	mysqlDevHost := beego.AppConfig.String("mysql_dev_host")
	mysqlDevUsername := beego.AppConfig.String("mysql_dev_username")
	mysqlDevPassword := beego.AppConfig.String("mysql_dev_password")
	mysqlDevDatabase := beego.AppConfig.String("mysql_dev_database")

	return map[string]interface{}{
		"Default":         defaultDb, //"mssql_dev", // 默认数据库配置
		"SetMaxOpenConns": 0,         // (连接池)最大打开的连接数，默认值为0表示不限制
		"SetMaxIdleConns": 1,         // (连接池)闲置的连接数, 默认1

		"Connections": map[string]map[string]string{
			"mysql_dev": { // 定义名为 mysql_dev 的数据库配置
				"host":     mysqlDevHost,     //"120.92.74.218", // 数据库地址
				"username": mysqlDevUsername, //"bpqa",          // 数据库用户名
				"password": mysqlDevPassword, //"123123qq",      // 数据库密码
				"port":     "3306",      // 端口
				"database": mysqlDevDatabase, //"bpqa_db",       // 链接的数据库名字
				"charset":  "utf8",      // 字符集
				"protocol": "tcp",       // 链接协议
				// "prefix": "",              // 表前缀
				"driver": "mysql", // 数据库驱动(mysql,sqlite,postgres,oracle,mssql)
			},
			"mssql_prod": { // 定义名为 mssql_prod 的数据库配置
				"host":     prodHost,     //"120.92.74.218", // 数据库地址
				"username": prodUsername, //"bpqa",          // 数据库用户名
				"password": prodPassword, //"123123qq",      // 数据库密码
				"port":     "1433",       // 端口
				"database": prodDatabase, //"bpqa_db",       // 链接的数据库名字
				"charset":  "utf8",       // 字符集
				"protocol": "tcp",        // 链接协议
				// "prefix": "",              // 表前缀
				"driver": "mssql", // 数据库驱动(mysql,sqlite,postgres,oracle,mssql)
			},
			"mssql_dev": { // 定义名为 mssql_dev 的数据库配置
				"host":     devHost,     //"120.92.74.218", // 数据库地址
				"username": devUsername, //"bpqa",          // 数据库用户名
				"password": devPassword, //"123123qq",      // 数据库密码
				"port":     "1433",      // 端口
				"database": devDatabase, //"bpqa_db",       // 链接的数据库名字
				"charset":  "utf8",      // 字符集
				"protocol": "tcp",       // 链接协议
				// "prefix": "",              // 表前缀
				"driver": "mssql", // 数据库驱动(mysql,sqlite,postgres,oracle,mssql)
			},
			"sqlite_dev": {
				"database": "./foo.db",
				"prefix":   "",
				"driver":   "sqlite3",
			},
		},
	}
}

// 获取DB实例
func getDBConnection() (gorose.Connection, error) {
	// 获取配置
	dbConfig := getDbConfig()
	// 打开链接
	connection, err := gorose.Open(dbConfig)
	if err != nil {
		return gorose.Connect, errors.New(err.Error())
	}
	return connection, err
}