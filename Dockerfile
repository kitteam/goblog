#源镜像
FROM golang:latest
#作者
MAINTAINER Razil "zheng_jinfan@126.com"
#设置工作目录
WORKDIR $GOPATH/src/gitee.com/kitteam/goblog
#将服务器的go工程代码加入到docker容器中
ADD . $GOPATH/src/gitee.com/kitteam/goblog

#设置时区
RUN /bin/cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
RUN echo 'Asia/Shanghai' >/etc/timezone

#安装依赖包
RUN go get github.com/astaxie/beego

#go构建可执行文件
RUN go build .
#暴露端口
EXPOSE 80
#最终运行docker的命令
ENTRYPOINT  ["./goblog"]